/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    validator,
} from "tripetto-runner-foundation";
import { TValidation } from "./validation";
import "./condition";

const IS_RFC822 = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const IS_RFC5322 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export abstract class Email extends NodeBlock<{
    validation?: TValidation;
}> {
    /** Contains the email slot with the value. */
    readonly emailSlot = assert(this.valueOf<string, Slots.String>("email"));

    /** Contains if the block is required. */
    readonly required = this.emailSlot.slot.required || false;

    @validator
    validate(): boolean {
        return (
            !this.emailSlot.string ||
            (this.props.validation === "rfc822" ? IS_RFC822 : IS_RFC5322).test(
                this.emailSlot.value
            )
        );
    }
}
