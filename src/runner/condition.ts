/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Str,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { TConditionMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class EmailCondition extends ConditionBlock<{
    mode: TConditionMode;
    match?: string;
}> {
    @condition
    isMatch(): boolean {
        const emailSlot = this.valueOf<string>();

        if (emailSlot) {
            const value = Str.lowercase(emailSlot.value);

            switch (this.props.mode) {
                case "domain":
                case "not-domain":
                    return (
                        (value.substr(value.lastIndexOf("@") + 1) ===
                            Str.lowercase(
                                this.parseVariables(this.props.match || "")
                            ) || false) ===
                        (this.props.mode === "domain" ? true : false)
                    );
                case "address":
                case "not-address":
                    return (
                        (value ===
                            Str.lowercase(
                                this.parseVariables(this.props.match || "")
                            ) || false) ===
                        (this.props.mode === "address" ? true : false)
                    );
                case "defined":
                    return value !== "";
                case "undefined":
                    return value === "";
            }
        }

        return false;
    }
}
