export type TConditionMode =
    | "address"
    | "not-address"
    | "domain"
    | "not-domain"
    | "defined"
    | "undefined";
