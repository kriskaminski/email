/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    slotInsertAction,
    tripetto,
} from "tripetto";
import { Email } from "./";
import { TConditionMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Email,
    icon: ICON,
    get label() {
        return pgettext("block:email", "Match email address");
    },
})
export class EmailCondition extends ConditionBlock {
    @definition
    @affects("#name")
    mode: TConditionMode = "address";

    @definition
    @affects("#name")
    match?: string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "domain":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "from domain"
                    )} ${this.match ? `\`\\@${this.match}\`` : "\\_"}`;
                case "not-domain":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "not from domain"
                    )} ${this.match ? `\`\\@${this.match}\`` : "\\_"}`;
                case "address":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "matches"
                    )} ${this.match ? `\`${this.match}\`` : "\\_"}`;
                case "not-address":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "does not match"
                    )} ${this.match ? `\`${this.match}\`` : "\\_"}`;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "specified"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:email",
                        "not specified"
                    )}`;
            }
        }

        return this.match || this.type.label;
    }

    @editor
    defineEditor(): void {
        const match = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "match", undefined, "")
        )
            .action("@", slotInsertAction(this, "exclude"))
            .autoFocus()
            .visible(this.mode !== "defined" && this.mode !== "undefined");

        this.editor.form({
            title: pgettext("block:email", "When value:"),
            controls: [
                new Forms.Radiobutton<TConditionMode>(
                    [
                        {
                            label: pgettext("block:email", "Matches address"),
                            value: "address",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Does not match address"
                            ),
                            value: "not-address",
                        },
                        {
                            label: pgettext("block:email", "Matches domain"),
                            value: "domain",
                        },
                        {
                            label: pgettext(
                                "block:email",
                                "Does not match domain"
                            ),
                            value: "not-domain",
                        },
                        {
                            label: pgettext("block:email", "Is specified"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:email", "Is not specified"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "address")
                ).on((mode: Forms.Radiobutton<TConditionMode>) => {
                    match.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                }),
                match,
            ],
        });
    }
}
