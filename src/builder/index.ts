/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    NodeBlock,
    Slots,
    conditions,
    each,
    editor,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { EmailCondition } from "./condition";
import { TConditionMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:email", "Email address");
    },
})
export class Email extends NodeBlock {
    emailSlot!: Slots.String;

    @slots
    defineSlot(): void {
        this.emailSlot = this.slots.static({
            type: Slots.String,
            reference: "email",
            label: Email.label,
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(this.emailSlot);
        this.editor.visibility();
        this.editor.alias(this.emailSlot);
        this.editor.exportable(this.emailSlot);
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    mode: "address" as "address",
                    label: pgettext("block:email", "Email address match"),
                },
                {
                    mode: "not-address" as "not-address",
                    label: pgettext("block:email", "Email address mismatch"),
                },
                {
                    mode: "domain" as "domain",
                    label: pgettext("block:email", "Email address on domain"),
                },
                {
                    mode: "not-domain" as "not-domain",
                    label: pgettext(
                        "block:email",
                        "Email address not on domain"
                    ),
                },
                {
                    mode: "defined" as "defined",
                    label: pgettext(
                        "block:email",
                        "Email address is specified"
                    ),
                },
                {
                    mode: "undefined" as "undefined",
                    label: pgettext(
                        "block:email",
                        "Email address is not specified"
                    ),
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: EmailCondition,
                    label: condition.label,
                    props: {
                        slot: this.emailSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}
